#!/bin/bash

path="/home/pascal/screenshots/$(date +%Y%m%d%H%M%S)_maim.png"
maim -s $path
notify-send "screenshot taken: $path"
